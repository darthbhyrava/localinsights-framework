import os
import unittest

from scraper.call_to_proxy_api import persist_today
from scraper.call_to_proxy_api import callonceperday

class TestModuleFunctions(unittest.TestCase):

    def test_persist_today(self):
        testfile = 'testfile.txt'
        res = persist_today(api_called_filename=testfile)
        self.assertIsNone(res)
        res = persist_today(api_called_filename=testfile)
        self.assertTrue(res)

        os.remove(testfile)

    def test_callonce_decorator(self):

        @callonceperday
        def test_fn():
            return 'x'

        res = test_fn()
        self.assertEqual(res, 'x')

        res = test_fn()
        self.assertIsNone(res)

        os.remove('api_called_filename.log')


if __name__ == '__main__':
    unittest.main()
