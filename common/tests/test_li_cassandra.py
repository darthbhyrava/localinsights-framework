"""
the tests here are mainly functional tests between the scraper and cassandra
"""

from scraper.li_cassandra import push_to_cassandra
from scraper.li_cassandra import timer


import unittest
import time


class Test_Cassandra(unittest.TestCase):

    def test_generate_response(self):
        @timer
        def slow_function():
            time.sleep(2)

        res = slow_function()
        self.assertTrue(res.get('data_pushed'))
        self.assertTrue(res.get('time_taken'))

    def test_push_to_cassandra_csvfilearg(self):
        res = push_to_cassandra(csvfile='tests/contracosta_05052005.csv')
        self.assertTrue(res.get('time_taken'))
        self.fail()

    def test_push_to_cassandra_datastream(self):
        res = push_to_cassandra(data_stream=[{'apn': '000000'}])
        self.assertTrue(res.get('time_taken'))
        self.fail()


if __name__ == '__main__':
    unittest.main()
