# remember to patch first
from gevent import monkey
monkey.patch_all()

import gevent

from uuid import uuid4
import time
import csv
from functools import wraps
import os

from cassandra.cqlengine.connection import setup
from cassandra.cqlengine.columns import UUID
from cassandra.cqlengine.columns import Text
from cassandra.cqlengine.models import Model
from cassandra.cqlengine.management import sync_table, drop_table, create_keyspace_simple, drop_keyspace
from gevent.pool import Pool

from scraper.exceptions import ScraperError

def timer(f):
    # we will fetch the timer and give that as a response
    @wraps(f)
    def wrapper(*args, **kwargs):
        t1 = time.time()
        f(*args, **kwargs)
        t2 = time.time()
        return {"data_pushed": True, "time_taken": (t2 - t1)}
    return wrapper


KEYSPACE = 'localinsightsv42'
# KEYSPACE = 'test_li'
TABLENAME= 'county_recorder'


class User(Model):
    __keyspace__ = KEYSPACE
    __table_name__ = TABLENAME

    user_id = UUID(primary_key=True)
    apn = Text()
    doc_number = Text()
    name = Text()
    block = Text()
    county = Text()
    doc_type = Text()
    gcp_link = Text()
    lot = Text()
    record_date = Text()
    role = Text()
    state = Text()
    transfer_amount = Text()

@timer
def push_to_cassandra(data_stream=None, csvfile=None, prod_server=None):
    """reads data from datastream and pushes to the cassandra server

    Usage::

        >>> from scraper.li_cassandra import push_to_cassandra
        >>> push_to_cassandra(['apn': something, 'doc_num': something])

    You can also pass a csv file

        >>> push_to_cassandra(csvfile='path/to/file.csv')

    list and csvfile do not work together. The first preference is for list
    and then it tries to see if any csvfile is present

    :param data_stream: A list of dicts.
    """
    if prod_server:
        setup([prod_server], KEYSPACE)
    else:
        setup(["127.0.0.1"], KEYSPACE)
    create_keyspace_simple(KEYSPACE, replication_factor=1)
    # drop_table(User)
    sync_table(User)

    # get the data from the csv
    if data_stream and isinstance(data_stream, list):
        reader = data_stream
    elif csvfile and os.path.isfile(csvfile):
        reader = csv.DictReader(open(csvfile))
    else:
        raise ScraperError('neither data_stream nor csvfile was passed')

    # create more users
    pool = Pool(100)
    # with Timer("create users async") as t:
    def create_user(row):
        try:
            user = User.create(user_id=uuid4(),
                            apn=row.get('apn'),
                            doc_number=row.get('doc_number'),
                            name=row.get('name'),
                            block=row.get('block'),
                            county=row.get('county'),
                            doc_type=row.get('doc_type'),
                            gcp_link=row.get('gcp_link'),
                            lot=row.get('lot'),
                            record_date=row.get('record_date'),
                            role=row.get('role'),
                            state=row.get('state'),
                            transfer_amount=row.get('transfer_amount'))
        except Exception as e:
            raise e

    pool.map(create_user, reader)


if __name__ == '__main__':
    # test runner
    print(push_to_cassandra('contracosta_05052005.csv'))
