import threading
import time
import random
import queue
import requests
from functools import wraps
import datetime
import os


FUZZ = False
api_called_filename = 'api_called_filename.log'


def fuzz():
    '''
    Fuzzing is a technique for amplifying race condition errors to make them
    more visible
    '''
    if FUZZ:
        time.sleep(random.random())


urls = [
    ('http://api.buyproxies.org/'
     '?a=showProxies&'
     'pid=73295&'
     'key=8e07d7606890447dc6961842caaf9fb9'),
]


def persist_today(api_called_filename=api_called_filename):
    now = datetime.datetime.now()
    today = '%s-%s-%s' % (now.year, now.month, now.day)

    if not os.path.isfile(api_called_filename):
        with open(api_called_filename, 'w+') as fw:
            fw.write('')

    with open(api_called_filename) as f:
        last_called_date = f.read()

    if last_called_date == today:
        return True
    else:
        with open(api_called_filename, 'w') as fw:
            fw.write(today)


def callonceperday(f):

    @wraps(f)
    def wrapper(*args, **kwargs):
        wrapper.called = persist_today()
        if not wrapper.called:
            return f(*args, **kwargs)
    wrapper.called = False
    return wrapper


class TaskHandler(object):
    """docstring for TaskHandler"""
    download_queue = None
    print_queue = None

    def download_manager(self):
        '''I have exclusive rights to update the download variable'''

        while True:
            url = self.download_queue.get()
            fuzz()
            response = requests.get(url, timeout=3)
            fuzz()
            self.print_queue.put([
                'downloaded content from url %s' % response.text,
            ])
            fuzz()
            self.download_queue.task_done()

    def print_manager(self):
        '''I have exclusive rights to call the "print" keyword'''

        while True:
            job = self.print_queue.get()
            fuzz()
            # for line in job:
            #     print(line, end='')
            #     fuzz()

            # now write to the file
            filename = 'buyproxies.txt'
            content = '\n'.join(job)
            with open(filename, 'a+') as fw:
                fw.write(content)
            fuzz()

            self.print_queue.task_done()
            fuzz()

    def worker(self, url):
        'My job is to increment the counter and print the current count'
        self.download_queue.put(url)
        fuzz()

    @callonceperday
    def start_the_tasks(self):
        # working with the download manager
        self.download_queue = queue.Queue()
        t = threading.Thread(target=self.download_manager)
        t.daemon = True
        t.start()
        del t

        # working with the print manager
        self.print_queue = queue.Queue()
        t = threading.Thread(target=self.print_manager)
        t.daemon = True
        t.start()
        del t

        # working with worker threads
        worker_threads = []
        for i, url in enumerate(urls):
            t = threading.Thread(target=self.worker, args=(url,))
            worker_threads.append(t)
            t.start()
            fuzz()

    def finishing_the_tasks(self):
        try:
            # finishing the tasks
            self.download_queue.join()
            fuzz()
            self.print_queue.join()
            fuzz()
        except AttributeError:
            # this is to take care of the case where no  objects get generated
            # because self has already run for today
            pass


if __name__ == '__main__':
    th = TaskHandler()
    th.start_the_tasks()
    th.finishing_the_tasks()
