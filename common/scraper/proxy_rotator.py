'''
to get the proxies from the api and server a random proxy

TODO:
    the api will get called each time the scraper object is built.
    we probably dont want that. We want the scraper object to 
    call the ProxyRotator only once
'''
import random
import os
import requests
from requests.exceptions import ConnectionError

from scraper.exceptions import ScraperError
from scraper.utils import file_is_empty


url = ('http://api.buyproxies.org/'
       '?a=showProxies&'
       'pid=73295&'
       'key=8e07d7606890447dc6961842caaf9fb9')
buyproxies = 'buyproxies.txt'

def download(url):
    # right now the architecture is flawed and this is not implementing
    # concurrency. Will need to refactor this code out and implement concurrency
    try:
        return requests.get(url, timeout=3)
    except ConnectionError:
        raise ScraperError('The API timedout')


def get_proxies_from_urls(url):
    '''download the proxies from the buyproxies api and return them'''
    response = download(url)
    proxies_from_buyproxies = response.text
    return proxies_from_buyproxies


def save_proxies_from_buyproxies(proxies_from_buyproxies):
    '''save the proxies to a buyproxies.txt text file'''
    with open(buyproxies, "w") as output:
        output.write(str(proxies_from_buyproxies))


class ProxyRotator(object):
    """This loads proxies from the proxy_list.txt file and returns a random one.

    Parameters:
    -----------
    path: str
        file name of the proxy list where the proxies are listed and delimited
        by a new line
    """

    # the path is hard coded. make this more modular
    def __init__(self, path='proxy_list.txt'):
        """Load proxy list from OS Path"""
        self.path = path
        if self.path == "proxy_list.txt":
            dir_path = os.path.dirname(os.path.realpath(__file__))
            proxy_list_path = "{dir_path}/{path}".format(dir_path=dir_path,
                                                         path=self.path)
        else:
            proxy_list_path = self.path
        self.proxy_list = self.__load_proxies(path=proxy_list_path)

    def handle_proxy_api_call(self):
        proxies_from_buyproxies = get_proxies_from_urls(url)
        save_proxies_from_buyproxies(proxies_from_buyproxies)
        self.change_buyproxies_to_proxy_list(buyproxies)

    def __load_proxies(self, path):
        """Load proxies given in proxy_list.txt"""
        proxies = []
        with open(path, 'r') as file:
            for line in file.readlines():
                proxies.append(line.strip())
        return proxies

    def change_buyproxies_to_proxy_list(self, buyproxies):
        '''if buyproxies get generated then rename it to the path of the file'''
        # once the buyproxies file gets generated in the next run the buyproxy
        # file will get converted to the actual path
        if not file_is_empty(buyproxies):
            os.rename(buyproxies, self.path)

    @property
    def proxies(self):
        # we will handle the proxy api call so that there is a call to the api
        # and the filename is changed
        self.handle_proxy_api_call()
        proxy = random.choice(self.proxy_list)

        proxies_dict = {
            'http' : proxy,
            'https': proxy
        }
        return proxies_dict
