from scraper import Scraper
import bs4
from bs4 import BeautifulSoup

# we initialise a bot that will have run the parsing functions
nobel = Scraper("nobel", start_date="3/12/2017", end_date="3/12/2017")

url = "https://en.wikipedia.org/wiki/List_of_Nobel_laureates"


def get_text(bs4_obj):
    if isinstance(bs4_obj, bs4.element.NavigableString):
        return str(bs4_obj)
    elif isinstance(bs4_obj, bs4.element.Tag):
        return bs4_obj.text
    else:
        return None


# below write the parsing functions for the data in the webpages for the webpage
@nobel.scrape(url)
def parse_page1():
    # We have the source of the page, let's ask BeaultifulSoup to parse it for
    # the bot we will look at the table and parse the individual items in it

    for body_row in nobel.find_elements_by_xpath("table tr"):
        cells = body_row.findAll('td')

        if len(cells) > 5:
            try:
                element_contents = [element.contents[0] for element in cells]
            except IndexError:
                pass
            element_contents = map(get_text, element_contents)
            elem = dict(zip(nobel.csvheaders, element_contents))
            yield elem


# put the functions in a list where the sequence should be in the order that you
# want the bot to crawl through the pages
parse_functions = [parse_page1]

# define the headers for the csv file that will be generated
nobel.csvheaders = ["Year",
                  "Physics",
                  "Chemistry",
                  "Medicine",
                  "Literature",
                  "Peace",
                  "Economics"]

if __name__ == "__main__":
    nobel.run(parse_functions)
