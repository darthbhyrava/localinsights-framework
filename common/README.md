# Bot Framework: Scraper

[comment]: <> ([![Build Status](https://travis-ci.org//pythondotorg.png?branch=master)](https://travis-ci.org/python/pythondotorg))
[comment]: <> ([![Documentation Status](https://readthedocs.org/projects/pythondotorg/badge/?version=latest)](http://pythondotorg.readthedocs.org/?badge=latest))

### General information

* Source code: https://bitbucket.org/lastlook1/localinsights-framework/src
* Issue tracker: 
* Documentation: please look into the docs in the docs folder
* Mailing list: 
* IRC: `#pydotorg` on Freenode
* Staging site: 
* License:


